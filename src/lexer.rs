use std::marker::PhantomData;
use std::sync::atomic::{AtomicUsize, Ordering};

use miette::{NamedSource, SourceSpan};
use miette::Diagnostic;
use nom::{
    branch::alt,
    bytes::complete::{escaped, tag},
    character::complete::{alphanumeric1, char, digit1, none_of, one_of},
    combinator::{eof, map, opt, recognize},
    multi::{many0, many1},
    sequence::{delimited, tuple},
    IResult,
};
use nom::character::complete::{anychar, line_ending, multispace0, multispace1};
use nom::multi::many_till;
use nom::sequence::{pair, terminated};
use thiserror::Error;

#[derive(Copy, Clone)]
pub struct Token<'a> {
    pub ty: TokenType,
    pub slice: &'a str,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum TokenType {
    // Symbols
    Asterisk,
    Backslash,
    Colon,
    Comma,
    Equals,
    Eroteme,
    Fwdslash,
    Hash,
    Minus,
    Period,
    Pipe,
    Plus,
    Semicolon,
    Underscore,

    // Braces
    LeftBrace,
    RightBrace,
    LeftBracket,
    RightBracket,
    LeftChevron,
    RightChevron,
    LeftParen,
    RightParen,

    // Keywords
    Async,
    As, // Order note: needs to be after Async, else Async will never be hit
    Enum,
    For,
    Fn,
    Loop,
    Struct,
    Try,
    Use,

    // Ident + Literals
    Comment,
    Ident,
    Number,
    Str,
}

#[derive(Debug, Error, Diagnostic)]
#[error("encountered an invalid token")]
pub struct InvalidToken {
    #[source_code]
    src: NamedSource,
    #[label("Here")]
    token: SourceSpan,
}

impl InvalidToken {
    fn new(src: NamedSource, start: usize, input: &str) -> InvalidToken {
        let mut i = 1;
        let slice_len = loop {
            let input = input.split_at(i).1;

            if all_valid_tokens(input).is_err() && my_space(input).is_err() {
                i += 1;
            } else {
                break i;
            }
        };

        InvalidToken { src, token: (start, slice_len).into() }
    }
}

pub struct Lexer<'a> {
    name: String,
    src: String,
    cursor: AtomicUsize,
    _tokens: PhantomData<&'a ()>
}

impl<'a> Lexer<'a> {
    pub fn new(name: String, src: String) -> Self {
        Self {
            name,
            src,
            cursor: AtomicUsize::new(0),
            _tokens: PhantomData,
        }
    }

    pub fn peak(&'a self) -> Result<Option<Token<'a>>, InvalidToken> {
        let (processed, mut current) = self.src.split_at(self.cursor.load(Ordering::Acquire));

        if is_end_of_tokens(current).is_ok() {
            return Ok(None);
        }

        let start = processed.len();

        if let Ok((input, _slice)) = eat_whitespace(current) {
            current = input;
        }

        match all_valid_tokens(current) {
            Ok((_, (slice, ty))) => Ok(Some(Token { slice, ty })),
            Err(nom::Err::Failure(nom::error::Error { input, ..})) | Err(nom::Err::Error(nom::error::Error { input, ..})) => {
                let named_src = NamedSource::new(self.name.clone(), self.src.clone());

                Err(InvalidToken::new(named_src, start, input))
            }
            Err(nom::Err::Incomplete(_)) => unreachable!()

        }
    }

    pub fn next(&'a self) -> Result<Option<Token<'a>>, InvalidToken> {
        let (processed, mut current) = self.src.split_at(self.cursor.load(Ordering::Acquire));

        if is_end_of_tokens(current).is_ok() {
            self.cursor.store(self.src.len(), Ordering::Release);
            return Ok(None);
        }

        let start = processed.len();

        if let Ok((input, slice)) = eat_whitespace(current) {
            self.cursor.fetch_add(slice.len(), Ordering::Acquire);
            current = input;
        }

        match all_valid_tokens(current) {
            Ok((_, (slice, ty))) => {
                self.cursor.fetch_add(slice.len(), Ordering::Acquire);
                Ok(Some(Token { slice, ty }))
            },
            Err(nom::Err::Failure(nom::error::Error { input, ..})) | Err(nom::Err::Error(nom::error::Error { input, ..})) => {
                let named_src = NamedSource::new(self.name.clone(), self.src.clone());

                Err(InvalidToken::new(named_src, start, input))
            }
            Err(nom::Err::Incomplete(_)) => unreachable!()
        }
    }
}

// Misc for glue purposes{

fn eat_whitespace(current: &str) -> IResult<&str, &str> {
    multispace1(current)
}

fn is_end_of_tokens(input: &str) -> IResult<&str, &str> {
    terminated(multispace0, eof)(input)
}

fn my_space(input: &str) -> IResult<&str, &str> {
    alt((multispace1, eof))(input)
}

fn all_valid_tokens(input: &str) -> IResult<&str, (&str, TokenType)> {
    alt((
        grp_brackets,
        grp_keywords,
        grp_ident_literals,
        grp_symbols,
    ))(input)
}

fn grp_symbols(input: &str) -> IResult<&str, (&str, TokenType)> {
    alt((
        sym_asterisk,
        sym_backslash,
        sym_colon,
        sym_comma,
        sym_equals,
        sym_eroteme,
        sym_fwdslash,
        sym_hash,
        sym_minus,
        sym_period,
        sym_pipe,
        sym_plus,
        sym_semicolon,
        sym_underscore,
    ))(input)
}

fn grp_brackets(input: &str) -> IResult<&str, (&str, TokenType)> {
    alt((brk_brace, brk_bracket, brk_chevron, brk_paren))(input)
}

fn grp_keywords(input: &str) -> IResult<&str, (&str, TokenType)> {
    alt((kw_async, kw_as, kw_enum, kw_for, kw_fn, kw_loop, kw_struct, kw_try, kw_use))(input)
}

fn grp_ident_literals(input: &str) -> IResult<&str, (&str, TokenType)> {
    alt((comment, ident, num_literal, str_literal))(input)
}

// Symbols

fn sym_asterisk(input: &str) -> IResult<&str, (&str, TokenType)> {
    tag("*")(input).map(|(i, s)| (i, (s, TokenType::Asterisk)))
}

fn sym_backslash(input: &str) -> IResult<&str, (&str, TokenType)> {
    tag("\\")(input).map(|(i, s)| (i, (s, TokenType::Backslash)))
}

fn sym_colon(input: &str) -> IResult<&str, (&str, TokenType)> {
    tag(":")(input).map(|(i, s)| (i, (s, TokenType::Colon)))
}

fn sym_comma(input: &str) -> IResult<&str, (&str, TokenType)> {
    tag(",")(input).map(|(i, s)| (i, (s, TokenType::Comma)))
}

fn sym_equals(input: &str) -> IResult<&str, (&str, TokenType)> {
    tag("=")(input).map(|(i, s)| (i, (s, TokenType::Equals)))
}

fn sym_eroteme(input: &str) -> IResult<&str, (&str, TokenType)> {
    tag("?")(input).map(|(i, s)| (i, (s, TokenType::Eroteme)))
}

fn sym_fwdslash(input: &str) -> IResult<&str, (&str, TokenType)> {
    tag("/")(input).map(|(i, s)| (i, (s, TokenType::Fwdslash)))
}

fn sym_hash(input: &str) -> IResult<&str, (&str, TokenType)> {
    tag("#")(input).map(|(i, s)| (i, (s, TokenType::Hash)))
}

fn sym_minus(input: &str) -> IResult<&str, (&str, TokenType)> {
    tag("-")(input).map(|(i, s)| (i, (s, TokenType::Minus)))
}

fn sym_period(input: &str) -> IResult<&str, (&str, TokenType)> {
    tag(".")(input).map(|(i, s)| (i, (s, TokenType::Period)))
}

fn sym_pipe(input: &str) -> IResult<&str, (&str, TokenType)> {
    tag("|")(input).map(|(i, s)| (i, (s, TokenType::Pipe)))
}

fn sym_plus(input: &str) -> IResult<&str, (&str, TokenType)> {
    tag("+")(input).map(|(i, s)| (i, (s, TokenType::Plus)))
}

fn sym_semicolon(input: &str) -> IResult<&str, (&str, TokenType)> {
    tag(";")(input).map(|(i, s)| (i, (s, TokenType::Semicolon)))
}

fn sym_underscore(input: &str) -> IResult<&str, (&str, TokenType)> {
    tag("_")(input).map(|(i, s)| (i, (s, TokenType::Underscore)))
}


// Brackets

fn brk_brace(input: &str) -> IResult<&str, (&str, TokenType)> {
    alt((
        map(tag("{"), |t| (t, TokenType::LeftBrace)),
        map(tag("}"), |t| (t, TokenType::RightBrace)),
    ))(input)
}

fn brk_bracket(input: &str) -> IResult<&str, (&str, TokenType)> {
    alt((
        map(tag("["), |t| (t, TokenType::LeftBracket)),
        map(tag("]"), |t| (t, TokenType::RightBracket)),
    ))(input)
}

fn brk_chevron(input: &str) -> IResult<&str, (&str, TokenType)> {
    alt((
        map(tag("<"), |t| (t, TokenType::LeftChevron)),
        map(tag(">"), |t| (t, TokenType::RightChevron)),
    ))(input)
}

fn brk_paren(input: &str) -> IResult<&str, (&str, TokenType)> {
    alt((
        map(tag("("), |t| (t, TokenType::LeftParen)),
        map(tag(")"), |t| (t, TokenType::RightParen)),
    ))(input)
}

// Keywords

fn kw_async(input: &str) -> IResult<&str, (&str, TokenType)> {
    tag("async")(input).map(|(i, s)| (i, (s, TokenType::Async)))
}

fn kw_as(input: &str) -> IResult<&str, (&str, TokenType)> {
    tag("as")(input).map(|(i, s)| (i, (s, TokenType::As)))
}

fn kw_enum(input: &str) -> IResult<&str, (&str, TokenType)> {
    tag("enum")(input).map(|(i, s)| (i, (s, TokenType::Enum)))
}

fn kw_for(input: &str) -> IResult<&str, (&str, TokenType)> {
    tag("for")(input).map(|(i, s)| (i, (s, TokenType::For)))
}

fn kw_fn(input: &str) -> IResult<&str, (&str, TokenType)> {
    tag("fn")(input).map(|(i, s)| (i, (s, TokenType::Fn)))
}

fn kw_loop(input: &str) -> IResult<&str, (&str, TokenType)> {
    tag("loop")(input).map(|(i, s)| (i, (s, TokenType::Loop)))
}

fn kw_struct(input: &str) -> IResult<&str, (&str, TokenType)> {
    tag("struct")(input).map(|(i, s)| (i, (s, TokenType::Struct)))
}

fn kw_try(input: &str) -> IResult<&str, (&str, TokenType)> {
    tag("try")(input).map(|(i, s)| (i, (s, TokenType::Try)))
}

fn kw_use(input: &str) -> IResult<&str, (&str, TokenType)> {
    tag("use")(input).map(|(i, s)| (i, (s, TokenType::Use)))
}

// Ident and literals

fn comment(input: &str) -> IResult<&str, (&str, TokenType)> {
    let (input, slice) = recognize(pair(tag("//"), many_till(anychar, line_ending)))(input)?;
    Ok((input, (slice, TokenType::Comment)))
}

fn ident(input: &str) -> IResult<&str, (&str, TokenType)> {
    let (input, slice) = recognize(many1(alt((alphanumeric1, tag("_")))))(input)?;
    Ok((input, (slice, TokenType::Ident)))
}

fn str_literal(input: &str) -> IResult<&str, (&str, TokenType)> {
    let (input, slice) = recognize(delimited(
        char('"'),
        escaped(none_of("\\\""), '\\', tag("\"")),
        char('"'),
    ))(input)?;

    Ok((input, (slice, TokenType::Str)))
}

fn num_literal(input: &str) -> IResult<&str, (&str, TokenType)> {
    // 100_000.00_f64
    let suffix = tuple((opt(char('_')), one_of("uif"), digit1));
    let number = tuple((digit1, many0(alt((digit1, tag("_"))))));
    let decimal = tuple((char('.'), many1(alt((digit1, tag("_"))))));
    let (input, slice) = recognize(tuple((number, opt(decimal), opt(suffix))))(input)?;

    Ok((
        input,
        (slice, TokenType::Number)
    ))
}
