use miette::Diagnostic;
use thiserror::Error;

use crate::lexer::{Lexer, Token, TokenType, TokenType::*, InvalidToken};

// TODO: Expand local errors into structs w/ miette
#[derive(Debug, Error, Diagnostic)]
pub enum ParserError {
    #[error("")]
    LexerError(InvalidToken),
    #[error("unexpectedly hit EOF")]
    UnexpectedEOF,
    #[error("illegal token")]
    IllegalToken,
}

pub struct IllegalToken {
}

impl From<InvalidToken> for ParserError {
    fn from(err: InvalidToken) -> Self {
        ParserError::LexerError(err)
    }
}

pub struct Parser<'a> {
    lexer: Lexer<'a>,
    ast: Vec<Expression>,
}

impl<'a> Parser<'a> {
    pub fn new(lexer: Lexer<'a>) -> Self {
        Self {
            lexer,
            ast: vec![],
        }
    }

    pub fn next(&'a mut self) -> Result<ParserIter, ParserError> {
        let next = self.lexer_assume_next(None)?;

        let expr = match next.ty {
            Use => Expression::Import(self.parse_import_node(vec![])?),
            _ => return Err(ParserError::IllegalToken),
        };

        self.ast.push(expr.clone());

        Ok(ParserIter::Continue(expr))
    }

    fn lexer_assume_next(&'a self, ty: impl Into<Option<TokenType>>) -> Result<Token<'a>, ParserError> {
        let token = self.lexer.next()?.ok_or(ParserError::UnexpectedEOF)?;

        match ty.into() {
            Some(ty) if ty != token.ty => Err(ParserError::IllegalToken),
            _ => Ok(token),
        }
    }

    fn lexer_assume_peak(&'a self, ty: impl Into<Option<TokenType>>) -> Result<Token<'a>, ParserError> {
        let token = self.lexer.peak()?.ok_or(ParserError::UnexpectedEOF)?;

        match ty.into() {
            Some(ty) if ty != token.ty => Err(ParserError::IllegalToken),
            _ => Ok(token),
        }
    }

    fn parse_import_node(&'a self, path: Vec<String>) -> Result<ImportTree, ParserError> {
        // Starting either just after `use` or `{`
        let mut tree = vec![];

        // After `use` or `{` must be an Ident
        let mut ident = self.lexer_assume_next(Ident)?;
        let mut alias: Option<Token> = None;

        let appended_path = |ident: Token| -> Vec<String> {
            let mut path = path.clone();
            path.push(ident.slice.to_string());
            path
        };

        let mut expected = None;

        loop {
            let next = self.lexer_assume_next(expected)?;
            match next.ty {
                Comma => {
                    let alias = alias.take().map(|t| t.slice.to_string());

                    let leaf = Import {
                        path: appended_path(ident),
                        alias,
                    };

                    tree.push(ImportTree::Leaf(leaf));

                    ident = self.lexer_assume_next(Ident)?;
                }, 
                RightBrace => break,
                Colon => {
                    self.lexer_assume_next(Colon)?;

                    let peak = self.lexer_assume_peak(None)?;

                    match peak.ty {
                        Ident => { 
                            let branch = self.parse_import_node(appended_path(ident))?;
        
                            tree.push(branch);
                        },
                        LeftBrace => {
                            self.lexer_assume_next(LeftBrace)?;

                            let branch = self.parse_import_node(appended_path(ident))?;

                            tree.push(branch);
                        },
                        _ => return Err(ParserError::IllegalToken),
                    }
                },
                Semicolon if path.is_empty() => break,
                As => {
                    alias = Some(self.lexer_assume_next(Ident)?);

                    expected = Some(Comma)
                }
                _ => return Err(ParserError::IllegalToken),
            }
        }

        Ok(ImportTree::Block(path, tree))
    }
}

#[derive(Debug)]
pub enum ParserIter {
    /// Finished current iteration
    Continue(Expression),
    /// Completed iterating through
    Finished,
    NeedImport(Vec<String>),
}

#[derive(Clone, Debug)]
pub enum Expression {
    Import(ImportTree),
    Enum(Enum),
    Struct(Struct),
    Block(Vec<Expression>),
}

#[derive(Clone, Debug)]
pub enum ImportTree {
    Block(Vec<String>, Vec<ImportTree>),
    Leaf(Import),
}

#[derive(Clone, Debug, Default)]
pub struct Import {
    path: Vec<String>,
    alias: Option<String>,
}

#[derive(Clone, Debug)]
pub struct Enum;

#[derive(Clone, Debug)]
pub struct Struct;

