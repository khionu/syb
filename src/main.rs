use miette::Diagnostic;
use thiserror::Error;

mod lexer;
mod parser;

use crate::lexer::InvalidToken;

#[derive(Debug, Diagnostic, Error)]
enum RootError {
    #[error("Encountered an IO error")]
    Io(#[from] std::io::Error),
    #[error("Encountered an error while lexing")]
    Lexer(#[from] InvalidToken),
}

#[tokio::main]
async fn main() -> miette::Result<()> {
    let lexer = crate::lexer::Lexer::new(String::new(), String::new());
    let mut parser = crate::parser::Parser::new(lexer);
    let _ = parser.next();
    Ok(())
}

#[cfg(test)]
mod tests {
    use std::fs::read_to_string;

    use crate::parser::ParserIter;

    #[test]
    fn lexer() {
        let source = read_to_string("samples/main.syb").unwrap();

        let lexer = crate::lexer::Lexer::new(String::from("main.syb"), source);

        while let Some(_) = lexer.next().expect("Failed to lex sample") { }
    }

    #[test]
    fn parser() {
        let source = read_to_string("samples/main.syb").unwrap();

        let lexer = crate::lexer::Lexer::new(String::from("main.syb"), source);
        let mut parser = crate::parser::Parser::new(lexer);

        loop {
            let loop_parser = &mut parser;
            let expr = loop_parser.next().expect("Failed to parse sample");
            println!("Expression parsed: {expr:?}");
            drop(loop_parser);
        }
    }
}
